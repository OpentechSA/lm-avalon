<?php

namespace App\Http\Controllers;

use DB;
use Log;
use PDO;
use Carbon\Carbon;
use App\Classes\DBHelpers;
use Illuminate\Http\Request;

class PersonasController extends Controller
{

    public function getDataPersona(Request $request)
    {

        try {

            $request = $request->all();

            $rs = DB::select("SELECT  AIAN8,ABTAX ,ABALPH,ALADD1,WPPH1 FROM CRPDTA.Vista_fd_F03012MM
                                WHERE AIAN8 = '" . $request['an8'] . "' OR ABTAX = '" . $request['cedula_ruc'] . "'
                                UNION
                            SELECT AIAN8, ABTAX,ABALPH,ALADD1,WPPH1 FROM PYNDTA.Vista_fd_F03012MM
                                WHERE AIAN8 = '" . $request['an8'] . "' OR ABTAX = '" . $request['cedula_ruc'] . "'");


            if ($rs) {
                $response = [
                    'code' => 200,
                    'message' => 'OK',
                    'datas' => $rs[0],
                ];

                return response(
                    $response,
                    200
                )->header('Content-Type', 'application/json');
            }

            return response()->json([
                'code' => 400,
                'message' => 'No se encontraron datos',
                'datas' => [],
            ]);
        } catch (\Throwable $th) {

            return response()->json([
                'code' => 400,
                'message' => $th->getMessage(),
                'datas' => [],
            ]);
        }
    }
}
