<?php

namespace App\Http\Controllers;

use DB;
use Log;
use PDO;
use Carbon\Carbon;
use App\Classes\DBHelpers;
use Illuminate\Contracts\Validation\Rule;
use Illuminate\Http\Request;

class ClientesController extends Controller
{

    public function slcar(Request $request)
    {

        try {

            $request = $request->all();

            $sqlAR = "SELECT RPAN8 AS AN8CLIENTE, RPRMR1 AS RUC, RPALPH AS CLIENTE, rpvr01 AS FACTURA, RPKCO AS CIA, rpglc as  LM, RPRYIN AS INSTPAGO, to_char(to_gregoriana(rpdivj)) AS FECHAFACTURA, (RPAG/100) AS IMPORTEBRUTOGUA, (RPAAP/100) AS IMPORTEPENDIENTEGUA, (rpacr/100) as IMPORTEBRUTOUSD, (RPFAP/100) AS IMPORTEPENDIENTEUsd";
            $sqlAR .= " FROM CRPDTA.F03B11@dbjde";
            $sqlAR .= " WHERE rpdct in( 'T1','T2') and (RPAAP) > 0";
            $sqlAR .= " and (to_char(to_gregoriana(rpdivj),'YYYYMMDD')) <= '" . $request['fechaHasta'] . "'";

            if ($request['instrumentoPago'] != "") {
                $sqlAR .= " AND RPRYIN = '" . $request['instrumentoPago'] . "'";
            } else {
                $sqlAR .= " AND RPRYIN IN ('6', '7','E')";
            }

            if ($request['ruc'] != "") {
                $sqlAR .= " AND RPRMR1 = '" . $request['ruc'] . "'";
            }

            if ($request['an8'] != 0) {
                $sqlAR .= " AND rpan8 = '" . $request['an8'] . "'";
            }

            if ($request['cia'] != "") {
                $sqlAR .= " AND rpco = '000" . $request['cia'] . "'";
            }

            if ($request['fechaDesde'] != "") {
                $sqlAR .= " AND (to_char(to_gregoriana(rpdivj),'YYYYMMDD')) >= '" . $request['fechaDesde'] . "'";
            }

            $rs = DB::select($sqlAR);

            if (!$rs) {
                $response = [
                    'code' => 400,
                    'message' => 'No se encontraron datos',
                    'datas' => [],
                ];
            }

            if ($rs) {
                $response = [
                    'code' => 200,
                    'message' => 'OK',
                    'datas' => $rs[0],
                ];

                return response(
                    $response,
                    200
                )->header('Content-Type', 'application/json');
            }
        } catch (\Throwable $th) {

            return response()->json([
                'code' => 400,
                'message' => $th->getMessage(),
                'datas' => [],
            ]);
        }
    }

    public function slcap(Request $request)
    {

        try {
            $sqlAP  = " SELECT RPAN8 AS AN8CLIENTE, ABTAX AS RUC, ABALPH AS CLIENTE, RPVINV AS FACTURA, RPCO AS CIA, RPPST AS ESTADOPAGO, to_char(to_gregoriana(rpdivj)) AS FECHAFACTURA, ((RPAG/100)*-1) AS IMPORTEBRUTOGUA, ((RPAAP/100)*-1) AS IMPORTEPENDIENTEGUA, ((rpacr/100)*-1) as IMPORTEBRUTOUSD, ((RPFAP/100)*-1) AS IMPORTEPENDIENTEUsd, RPGLC AS COMPLM, RPPO as ORDENCOMPRA, RPRMK AS OBSERVACION";
            $sqlAP .= " FROM CRPDTA.F0411@dbjde,CRPDTA.F0101@dbjde";
            $sqlAP .= " WHERE ABAN8 = RPAN8 and RPDCT in ('PV','PD','P$') AND RPPST in ('L','C') AND ((RPAAP)<> 0 OR (RPFAP)<> 0)";
            $sqlAP .= " and RPGLC in ('PLCB','PLCZ','PLCP') and (to_char(to_gregoriana(rpdivj),'YYYYMMDD')) <= '" . $request['fechaHasta'] . "'";
            if ($request['an8'] != 0) {
                $sqlAP .= " AND RPAN8 = '" . $request['an8'] . "'";
            }

            if ($request['cia'] != "") {
                $sqlAP .= " AND RPCO = '000" . $request['cia'] . "'";
            }

            if ($request['ruc'] != "") {
                $sqlAP .= " AND ABTAX = '" . $request['ruc'] . "'";
            }

            if ($request['fechaDesde'] != "") {
                $sqlAP .= " AND (to_char(to_gregoriana(rpdivj),'YYYYMMDD')) >= '" . $request['fechaDesde'] . "'";
            }

            $rs = DB::select($sqlAP);

            if (!$rs) {
                $response = [
                    'code' => 400,
                    'message' => 'No se encontraron datos',
                    'datas' => [],
                ];
            }

            if ($rs) {
                $response = [
                    'code' => 200,
                    'message' => 'OK',
                    'datas' => $rs[0],
                ];

                return response(
                    $response,
                    200
                )->header('Content-Type', 'application/json');
            }
        } catch (\Throwable $th) {

            return response()->json([
                'code' => 400,
                'message' => $th->getMessage(),
                'datas' => [],
            ]);
        }
    }
}
