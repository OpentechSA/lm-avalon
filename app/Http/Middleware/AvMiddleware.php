<?php

namespace App\Http\Middleware;

use Closure;

class AvMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */

    public function handle($request, Closure $next)
    {
        $token = $request->bearerToken();
        
        if(!empty($token) ? $token !== env('TOKEN') : true)
        {
            return response()->json([
                'code' => 401,
                'message' => 'No autorizado',
                'datas' => [],
            ]);
        }

        return $next($request);
    }

}
