<?php

namespace App\Classes;

use DB;
use PDO;
use Log;
use Carbon\Carbon;

class DBHelpers
{
    public static function executeProcedure($procedureName, array $bindings = [], $call_key = null){
        
        $pdo = DB::getPdo();
        $paramNames = implode(',:',array_column($bindings,'name'));
        $stmt = $pdo->prepare("begin $procedureName(:$paramNames); end;");
        $cursors = [];
        $result = [];

        Log::info($call_key.'_request:'.json_encode($bindings));

        foreach ($bindings as $key => $row){
             // binding input parameters.
            if(isset($row['value']))
                $stmt->bindParam(":".$row['name'], $row['value']);
            
            // binding cursor output parameters.
            else if($row['type'] === PDO::PARAM_STMT)
                $stmt->bindParam(":".$row['name'],  $cursors[$row['name']], PDO::PARAM_STMT);

            // binding other none cursor output parameters.
            else
                $stmt->bindParam(":".$row['name'],  $result[$row['name']], $row['type']);
        }

        $stmt->execute();

        //fetch cursors and put them inside result
        foreach($cursors as $key => $cursor){
            oci_execute($cursor, OCI_DEFAULT);
            oci_fetch_all($cursor, $result[$key], 0, -1, OCI_FETCHSTATEMENT_BY_ROW + OCI_ASSOC );
            oci_free_cursor($cursor);
        }

        Log::info($call_key.'_response:'.json_encode($result));

        return $result;
    }
}